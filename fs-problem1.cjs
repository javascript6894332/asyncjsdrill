const fs = require("fs");
const path = require("path");

function createJsonFile(
  directoryPath,
  fileName,
  fileIndex,
  randomNumberOfFiles,
  callback
) {
  const filePath = path.join(directoryPath, fileName);
  const jsonData = {
    randomValue: Math.floor(Math.random() * 10) + 5,
  };

  fs.writeFile(filePath, JSON.stringify(jsonData, null, 2), (err) => {
    if (err) {
      callback(err);
    } else {
      console.log(`Created file: ${filePath}`);
      callback(
        deleteJsonFile(
          path.join(directoryPath, fileName),
          fileIndex,
          randomNumberOfFiles,
          (deletErr) => {
            if (deletErr) {
              console.log("Error deleting file: ", fileName);
            }
          }
        )
      );
    }
  });
}

function deleteJsonFile(filePath, fileIndex, randomNumberOfFiles, callback) {
  fs.unlink(filePath, (err) => {
    if (err) {
      console.log(`error encountered: ${err}`);
    } else {
      if (fileIndex === randomNumberOfFiles - 1) {
        console.log("All Files Deleted");
      }
    }
  });
}

function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
  fs.mkdir(absolutePathOfRandomDirectory, (error) => {
    if (error) {
      console.log(`${error} while creating directory`);
      return;
    } else {
      console.log(
        `directory succesfully created, creating ${randomNumberOfFiles} files now.`
      );

      for (let fileIndex = 0; fileIndex < randomNumberOfFiles; fileIndex++) {
        let fileName = `file${fileIndex}`;
        createJsonFile(
          absolutePathOfRandomDirectory,
          fileName,
          fileIndex,
          randomNumberOfFiles,
          (error) => {
            if (error) {
              console.log("error creating file", error);
            }
          }
        );
      }
    }
  });
}

module.exports = fsProblem1;
