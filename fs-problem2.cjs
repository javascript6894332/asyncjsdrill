const fs = require("fs");

function fsProblem2(filePath) {
  fs.readFile(filePath, "utf8", (err, data) => {
    if (err) {
      console.error("Error reading:", err);
      return;
    }

    const contentOfUpper = data.toUpperCase();
    const upperCaseFile = "uppercase.txt";

    fs.writeFile(upperCaseFile, contentOfUpper, (err) => {
      if (err) {
        console.error("Error in uppercase.txt:", err);
        return;
      } else {
        console.log(`written in ${upperCaseFile}.`)
      }

      fs.appendFile("filenames.txt", upperCaseFile + "\n", (err) => {
        if (err) {
          console.error("Error pusing to filenames.txt:", err);
          return;
        } else {
            console.log(`appending new file name to filenames.txt`)
        }

        fs.readFile(upperCaseFile, "utf8", (err, upperCaseTexts) => {
          if (err) {
            console.error("Error reading uppercase.txt:", err);
            return;
          }

          const contentOfLower = upperCaseTexts.toLowerCase();
          const lowerSentences = contentOfLower.split(/[.!?]/);
          const sentenceFileName = "lowerSentence.txt";

          fs.writeFile(sentenceFileName, lowerSentences.join("\n"), (err) => {
            if (err) {
              console.error("Error writing to lowerSentences.txt:", err);
              return;
            } else {
                console.log(`written to ${sentenceFileName}`)
            }

            fs.appendFile("filenames.txt", sentenceFileName + "\n", (err) => {
              if (err) {
                console.error("Error appending to filenames.txt:", err);
                return;
              }

              fs.readFile(sentenceFileName, "utf8", (err, sentenceData) => {
                if (err) {
                  console.error("Error reading lowerSentences.txt:", err);
                  return;
                }

                const sortedlowerSentences = sentenceData
                  .split("\n")
                  .sort()
                  .join("\n");
                const sortedFileName = "sorted.json";

                fs.writeFile(sortedFileName, sortedlowerSentences, (err) => {
                  if (err) {
                    console.error("Error writing to sorted.txt:", err);
                    return;
                  } else {
                    console.log(`written in ${sortedFileName}`)
                  }

                  fs.appendFile(
                    "filenames.txt",
                    sortedFileName + "\n",
                    (err) => {
                      if (err) {
                        console.error(
                          "Error appending ne file name to filenames.txt:",
                          err
                        );
                        return;
                      }

                      fs.readFile(
                        "filenames.txt",
                        "utf8",
                        (err, filenamesData) => {
                          if (err) {
                            console.error("Error reading filenames.txt:", err);
                            return;
                          }

                          const filenames = filenamesData
                            .split("\n")
                            .filter(Boolean);

                          filenames.forEach((filename) => {
                            fs.unlink(filename, (err) => {
                              if (err) {
                                console.error("Error deleting file:", err);
                              } else {
                                console.log(`deleted ${filename}`);
                              }
                            });
                          });

                          console.log("All tasks completed.");
                        }
                      );
                    }
                  );
                });
              });
            });
          });
        });
      });
    });
  });
}

module.exports = fsProblem2;
